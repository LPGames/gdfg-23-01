namespace Simplicity
{
    public class GameBlock
    {
        private BlockPosition _position;
        private IBlockWrapper _wrapper;

        public GameBlock(BlockPosition position)
        {
            _position = position;
        }

        public void SetWrapper(IBlockWrapper wrapper)
        {
            _wrapper = wrapper;
        }

        public IBlockWrapper GetWrapper()
        {
            return _wrapper;
        }

        public void MoveTo(BlockPosition newPosition)
        {
            _position = newPosition;

            if (_wrapper != null)
            {
                _wrapper.MoveBlock(_position);
            }

        }

        public BlockPosition GetPosition()
        {
            return _position;
        }

        public void Remove()
        {
            if (_wrapper != null)
            {
                _wrapper.RemoveBlock();
            }
        }
    }

    public interface IBlockWrapper
    {
        void MoveBlock(BlockPosition position);
        void RemoveBlock();
    }

    public struct BlockPosition
    {
        public int x;
        public int y;

        public BlockPosition(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public void Move(MoveDirection direction)
        {
            switch (direction)
            {
                case MoveDirection.Up:
                    y--;
                    break;
                case MoveDirection.Down:
                    y++;
                    break;
                case MoveDirection.Left:
                    x--;
                    break;
                case MoveDirection.Right:
                    x++;
                    break;
            }
        }
    }
}
