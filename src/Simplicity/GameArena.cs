using System;
using System.Collections.Generic;
using System.Linq;
using Godot;

namespace Simplicity
{
    public class GameArena
    {
        public static event Action<GameBlock> OnWallCreated;
        public static event Action<GameBlock> OnBlockCreated;
        public static event Action<GameBlock> OnWinBlockCreated;
        public static event Action OnBlocksMoved;

        private GameManager _gameManager;

        private int _arenaWidth = 9;
        private int _arenaHeight = 16;
        private int _randomOffset = 10;

        private bool _blocksCanMergeButNotCrack = true;

        private Dictionary<BlockPosition, GameBlock> _walls = new Dictionary<BlockPosition, GameBlock>();
        private Dictionary<BlockPosition, GameBlock> _crackableWalls = new Dictionary<BlockPosition, GameBlock>();
        private Dictionary<BlockPosition, GameBlock> _blocks = new Dictionary<BlockPosition, GameBlock>();
        private Dictionary<BlockPosition, GameBlock> _winBlocks = new Dictionary<BlockPosition, GameBlock>();

        public GameArena(GameManager gameManager)
        {
            _gameManager = gameManager;
        }


        public int GetWallCount()
        {
            return _walls.Count;
        }

        public int GetCrackableWallCount()
        {
            return _crackableWalls.Count;
        }

        public int GetWidth()
        {
            return _arenaWidth;
        }
        public void SetWidth(int width)
        {
            _arenaWidth = width;
        }

        public int GetHeight()
        {
            return _arenaHeight;
        }

        public void SetHeight(int height)
        {
            _arenaHeight = height;
        }

        public int GetBlockCount()
        {
            return _blocks.Count;
        }

        public int GetWinBlockCount()
        {
            return _winBlocks.Count;
        }

        public void SetBlocksAsBlockers(bool isBlocker)
        {
            _blocksCanMergeButNotCrack = isBlocker;
        }

        #region Setup

        public void CreateWalls(int x, int y)
        {
            _arenaWidth = x;
            _arenaHeight = y;

            for (int i = 0; i < _arenaWidth; i++)
            {
                for (int j = 0; j < _arenaHeight; j++)
                {
                    if (i == 0 || j == 0 || i == _arenaWidth - 1 || j == _arenaHeight - 1)
                    {
                        var position = new BlockPosition(i, j);
                        CreateWall(position);

                    }
                }
            }
        }


        public void CreateRandomBlocks(int maxBlocks)
        {
            RandomNumberGenerator rng = new RandomNumberGenerator();
            rng.Randomize();

            for (int i = 0; i < _arenaWidth; i++)
            {
                for (int j = 0; j < _arenaHeight; j++)
                {
                    if (_blocks.Count == maxBlocks)
                    {
                        break;
                    }

                    var position = new BlockPosition(i,j);
                    if (IsWallAtPosition(position, out GameBlock _))
                    {
                        continue;
                    }

                    int randomNumber = rng.RandiRange(0, _randomOffset);
                    if (randomNumber != _randomOffset)
                    {
                        continue;
                    }

                    CreateBlock(position);
                }
            }
        }

        public void CreateRandomWalls(int maxBlocks)
        {
            RandomNumberGenerator rng = new RandomNumberGenerator();
            rng.Randomize();

            int randomOffset = _arenaHeight * _arenaWidth / maxBlocks;

            int wallCount = 0;
            for (int i = 0; i < _arenaWidth; i++)
            {
                for (int j = 0; j < _arenaHeight; j++)
                {
                    if (wallCount == maxBlocks)
                    {
                        break;
                    }

                    if (i == 0 || j == 0 || i == _arenaWidth - 1 || j == _arenaHeight - 1)
                    {
                        continue;
                    }

                    var position = new BlockPosition(i,j);
                    if (IsWinBlockAtPostition(position))
                    {
                        continue;
                    }

                    int randomNumber = rng.RandiRange(0, randomOffset);
                    if (randomNumber != randomOffset)
                    {
                        continue;
                    }

                    CreateWall(position, true);
                    wallCount++;
                }
            }
        }

        public void CreateWinBlock(BlockPosition position)
        {
            var block = new GameBlock(position);
            OnWinBlockCreated?.Invoke(block);
            _winBlocks.Add(position, block);
        }

        public void CreateBlock(BlockPosition position)
        {
            var block = new GameBlock(position);
            OnBlockCreated?.Invoke(block);
            _blocks.Add(position, block);
        }

        public void CreateWall(BlockPosition position, bool crackable = false)
        {
            var block = new GameBlock(position);
            OnWallCreated?.Invoke(block);
            if (crackable)
            {
                _crackableWalls.Add(position, block);
            }
            else
            {
                _walls.Add(position, block);
            }
        }

        #endregion

        public GameBlock GetBlock(int delta)
        {
            return _blocks.Values.ElementAt(delta);
        }

        public GameBlock GetWinBlock(int delta)
        {
            return _winBlocks.Values.ElementAt(delta);
        }

        public GameBlock GetWall(int delta)
        {
            return _walls.Values.ElementAt(delta);
        }

        public GameBlock GetCrackableWall(int delta)
        {
            return _crackableWalls.Values.ElementAt(delta);
        }

        public void MoveBlocks(MoveDirection direction)
        {
            List<GameBlock> blocksToMove = _blocks.Values.ToList();
            foreach (var block in blocksToMove)
            {
                MoveBlock(block, direction);
            }

            OnBlocksMoved?.Invoke();
        }

        public void MoveBlock(GameBlock block, MoveDirection direction)
        {

            BlockPosition position = block.GetPosition();
            BlockPosition newPosition = position;

            _blocks.Remove(position);

            while (CanMoveBlockToPosition(newPosition))
            {
                position = newPosition;
                newPosition.Move(direction);

                if (_blocksCanMergeButNotCrack && IsBlockAtPosition(newPosition))
                {
                    // Try to move blocker.
                    GameBlock blocker = _blocks[newPosition];
                    MoveBlock(blocker, direction);

                    if (IsBlockAtPosition(newPosition))
                    {
                        // Blocker is still in the way.
                        break;
                    }
                }
            }

            block.MoveTo(position);

            if (IsWinBlockAtPostition(position))
            {
                block.Remove();
                return;
            }

            if (!IsBlockAtPosition(position))
            {
                _blocks.Add(position, block);
            }
            else
            {
                block.Remove();
            }
        }

        private bool CanMoveBlockToPosition(BlockPosition position)
        {
            bool isWall = IsWallAtPosition(position,  out GameBlock wall);

            if (!isWall)
            {
                return true;
            }

            // Not crackable wall.
            if (wall == null || !_blocksCanMergeButNotCrack)
            {
                return false;
            }

            var wallInstance = wall.GetWrapper() as Wall;
            if (wallInstance.IsCracked())
            {
                _crackableWalls.Remove(position);
                wallInstance.RemoveBlock();
                return true;
            }

            wallInstance.Crack();
            return false;
        }

        private bool IsWinBlockAtPostition(BlockPosition position)
        {
            return _winBlocks.ContainsKey(position);
        }

        private bool IsWallAtPosition(BlockPosition position, out GameBlock wall)
        {
            wall = null;
            if (_walls.ContainsKey(position))
            {
                return true;
            }

            if (_crackableWalls.ContainsKey(position))
            {
                wall = _crackableWalls[position];
                return true;
            }

            return false;
        }

        private bool IsBlockAtPosition(BlockPosition position)
        {
            return _blocks.ContainsKey(position);
        }
    }

    public class ArenaData
    {
        public int width;
        public int height;
        public List<BlockPosition> crackable = new List<BlockPosition>();
        public List<BlockPosition> walls = new List<BlockPosition>();
        public List<BlockPosition> blocks = new List<BlockPosition>();
        public List<BlockPosition> wins = new List<BlockPosition>();
        public string hint = "";

        public static ArenaData FromGameArena(GameArena arena)
        {
            ArenaData data = new ArenaData();
            data.width = arena.GetWidth();
            data.height = arena.GetHeight();
            for (int i = 0; i < arena.GetWallCount(); i++)
            {
                data.walls.Add(arena.GetWall(i).GetPosition());
            }
            for (int i = 0; i < arena.GetCrackableWallCount(); i++)
            {
                data.crackable.Add(arena.GetCrackableWall(i).GetPosition());
            }
            for (int i = 0; i < arena.GetBlockCount(); i++)
            {
                data.blocks.Add(arena.GetBlock(i).GetPosition());
            }
            for (int i = 0; i < arena.GetWinBlockCount(); i++)
            {
                data.wins.Add(arena.GetWinBlock(i).GetPosition());
            }
            return data;
        }
    }
}
