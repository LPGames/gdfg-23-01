using System;
using Newtonsoft.Json;
using Godot;

namespace Simplicity
{
    public class GameManager
    {
        public static event Action<string> OnShowHint;
        private GameArena _arena;
        private bool _gameOver;

        public GameManager()
        {
            _arena = new GameArena(this);
        }

        public void SetGameOver()
        {
            _gameOver = true;
        }

        public void SetupLevel(int x, int y)
        {
            _arena.CreateWalls(x, y);
        }

        public void AddRandomWalls(int maxQuantity)
        {
            _arena.CreateRandomWalls(maxQuantity);
        }

        public void AddRandomBlocks(int maxQuantity)
        {
            _arena.CreateRandomBlocks(maxQuantity);
        }

        public void AddWinBlock(int x, int y)
        {
            var position = new BlockPosition(x, y);
            _arena.CreateWinBlock(position);
        }

        public void AddBlock(BlockPosition position)
        {
            _arena.CreateBlock(position);
        }

        public int GetWallCount()
        {
            return _arena.GetWallCount();
        }

        public int GetBlockCount()
        {
            return _arena.GetBlockCount();
        }

        public void MoveBlocks(MoveDirection direction)
        {
            if (_gameOver)
            {
                return;
            }
            _arena.MoveBlocks(direction);
        }

        public GameBlock GetBlock(int delta)
        {
            return _arena.GetBlock(delta);
        }

        public void LoadLevelFromFile(string path)
        {
            var file = new File();
            file.Open(path, File.ModeFlags.Read);
            var json = file.GetAsText();
            file.Close();
            var arenaData = JsonConvert.DeserializeObject<ArenaData>(json);

            _arena.SetHeight(arenaData.height);
            _arena.SetWidth(arenaData.width);
            foreach (var blockPosition in arenaData.blocks)
            {
                _arena.CreateBlock(blockPosition);
            }

            foreach (var wallPosition in arenaData.walls)
            {
                _arena.CreateWall(wallPosition);
            }

            foreach (var crackableWallPosition in arenaData.crackable)
            {
                _arena.CreateWall(crackableWallPosition, true);
            }

            foreach (var winPosition in arenaData.wins)
            {
                _arena.CreateWinBlock(winPosition);
            }

            if (arenaData.hint.Length > 0)
            {
                OnShowHint?.Invoke(arenaData.hint);
            }
        }

        public void SaveLevel()
        {
            ArenaData arenaData = ArenaData.FromGameArena(_arena);
            string dataString = JsonConvert.SerializeObject(arenaData);
            File file = new File();
            file.Open("user://arena.json", File.ModeFlags.Write);
            file.StoreString(dataString);
            file.Close();
        }

        public void SetPlayMode(GamePlayMode playMode)
        {
            _arena.SetBlocksAsBlockers(playMode == GamePlayMode.Block);
        }
    }
}
