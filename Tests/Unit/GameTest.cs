using System.Collections.Generic;
using Simplicity;

namespace Tests
{
	[Title("Game Logic Test")]

	// See target methods for more information
	[Start(nameof(Setup))]
	[Pre(nameof(PrepareTest))]
	[Post(nameof(CleanupTest))]
	[End(nameof(Cleanup))]

	public class GameTest : WAT.Test
	{
		[Test(2, 2, 4)]
		[Test(4, 4, 12)]
		[Test(6, 6, 20)]
		public void SetupLevelWallsTest(int a, int b, int expected)
		{
			Describe("Check that different game sizes have the right number of walls.");

			GameManager gameManager = new GameManager();
			gameManager.SetupLevel(a, b);
			int walls = gameManager.GetWallCount();

			Assert.IsEqual(expected, walls, "There are " + expected + " wall blocks.");
		}

		[Test]
		public void SetupLevelBlocksTest()
		{
			Describe("Check that the correct number of walls are created.");

			GameManager gameManager = new GameManager();
			gameManager.SetupLevel(9, 16);
			gameManager.AddRandomBlocks(5);
			int blocks = gameManager.GetBlockCount();

			Assert.IsEqual(5, blocks, "There are 5 player blocks.");
		}

		[Test(MoveDirection.Up, 5, 5, 2, 2, 2, 1)]
		[Test(MoveDirection.Down, 5, 5, 2, 2, 2, 3)]
		[Test(MoveDirection.Left, 5, 5, 2, 2, 1, 2)]
		[Test(MoveDirection.Right, 5, 5, 2, 2, 3, 2)]
		[Test(MoveDirection.Up, 7, 7, 3, 3, 3, 1)]
		[Test(MoveDirection.Down, 7, 7, 3, 3, 3, 5)]
		[Test(MoveDirection.Left, 7, 7, 3, 3, 1, 3)]
		[Test(MoveDirection.Right, 7, 7, 3, 3, 5, 3)]
		public void MoveBlockTest(MoveDirection direction, int gameX, int gameY, int startX, int startY, int finalX, int finalY)
		{
			Describe("Check that a block can be moved.");

			GameManager gameManager = new GameManager();
			gameManager.SetupLevel(gameX, gameY);
			gameManager.AddBlock(new BlockPosition(startX, startY));

			gameManager.MoveBlocks(direction);

			var block = gameManager.GetBlock(0);
			var positionAfter = block.GetPosition();

			Assert.IsEqual(finalX, positionAfter.x, "The block moved horizontally.");
			Assert.IsEqual(finalY, positionAfter.y, "The block moved vertically.");
		}

		[Test(MoveDirection.Up, 2, 1, 2, 2)]
		[Test(MoveDirection.Down, 2, 2, 2, 3)]
		[Test(MoveDirection.Left, 1, 1, 1, 2)]
		[Test(MoveDirection.Right, 3, 1, 3, 2)]
		public void MoveTwoBlocksTest(MoveDirection direction, int finalOneX, int finalOneY, int finalTwoX, int finalTwoY)
		{
			Describe("Check that a block can be moved.");

			GameManager gameManager = new GameManager();
			gameManager.SetupLevel(5, 5);
			gameManager.SetPlayMode(GamePlayMode.Block);

			gameManager.AddBlock(new BlockPosition(2, 1));
			gameManager.AddBlock(new BlockPosition(2, 2));

			gameManager.MoveBlocks(direction);

			var blockOne = gameManager.GetBlock(0);
			var positionAfterOne = blockOne.GetPosition();

			var blockTwo = gameManager.GetBlock(1);
			var positionAfterTwo = blockTwo.GetPosition();

			Assert.IsEqual(finalOneX, positionAfterOne.x, "The block one moved horizontally.");
			Assert.IsEqual(finalOneY, positionAfterOne.y, "The block one moved vertically.");
			Assert.IsEqual(finalTwoX, positionAfterTwo.x, "The block two moved horizontally.");
			Assert.IsEqual(finalTwoY, positionAfterTwo.y, "The block two moved vertically.");
		}

		public void Setup()
		{
		}

		public void PrepareTest()
		{
		}

		public void CleanupTest()
		{
		}

		public void Cleanup()
		{
		}
	}
}
