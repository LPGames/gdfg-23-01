using Godot;
using System;
using Simplicity;

public class Interface : Control
{
    public static event Action<int> OnMovesChanged;
    public static event Action OnToggleButtonPressed;

    private ScoreManager _scoreManager;

    private Label _pointsText, _movesText;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        _scoreManager = GetNode<ScoreManager>("/root/ScoreManager");

        _pointsText = GetNode<Label>("Background/Points");
        _movesText = GetNode<Label>("Background/Moves");

        _pointsText.Text = "0";
        _movesText.Text = "0";
    }

    public override void _Process(float delta)
    {
        UpdateScore();
    }

    public void SetTopPoints()
    {
        int points = _scoreManager.GetTopScore();
        _pointsText.Text = "Top: " + points.ToString();
    }

    public void UpdateMoves()
    {
        bool success = Int32.TryParse(_movesText.Text, out int moves);
        if (success)
        {
            moves++;
            _movesText.Text = moves.ToString();
        }

        OnMovesChanged?.Invoke(moves);
    }

    private void UpdateScore()
    {
        _pointsText.Text = _scoreManager.GetScore().ToString();
    }

    public void ToggleButtonPressed()
    {
        OnToggleButtonPressed?.Invoke();
    }

    public override void _EnterTree()
    {
        GameArena.OnBlocksMoved += UpdateMoves;
    }

    public override void _ExitTree()
    {
        GameArena.OnBlocksMoved -= UpdateMoves;
    }
}
