using Godot;
using System;

public class LevelGroup : Control
{
    private Node _levelContainer;


    public override void _Ready()
    {
        _levelContainer = GetNode<Node>("LevelContainer");

        for (int i = 0; i < 20; i++)
        {
            AddLevel();
        }
    }

    public void AddLevel()
    {
        var level = GD.Load<PackedScene>(AssetReference.LevelSelectorPrefab).Instance();
        _levelContainer.AddChild(level);
    }
}
