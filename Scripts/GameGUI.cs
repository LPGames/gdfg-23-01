using Godot;
using System;
using Simplicity;

public class GameGUI : Control
{
    public override void _Ready()
    {
        // OnShowHint();
    }

    private void OnShowHint(string hint)
    {
        var hintScreen = (HintScreen) GD.Load<PackedScene>(AssetReference.HintScreenPrefab).Instance();
        hintScreen.SetHint(hint);
        AddChild(hintScreen);
    }


    public override void _EnterTree()
    {
        GameManager.OnShowHint += OnShowHint;
    }

    public override void _ExitTree()
    {
        GameManager.OnShowHint -= OnShowHint;
    }
}
