using Godot;

public class LevelSelection : UISceneBase
{
    private LevelManager _levelManager;

    public override void _Ready()
    {
        _levelManager = GetNode<LevelManager>("/root/LevelManager");
    }

    public override void _Input(InputEvent inputEvent)
    {
        if (inputEvent.IsActionPressed("escape"))
        {
            _levelManager.LoadMenuScene();
        }
    }

    public void OnBackButtonPressed()
    {
        _levelManager.LoadMenuScene();
    }
}
