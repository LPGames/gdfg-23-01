using Godot;
using Simplicity;

public class WinScreen : Control
{
    private GameManager _gameManager;
    private LevelManager _levelManager;
    private ScoreManager _scoreManager;
    private Label _scoreLabel;
    private Label _topScoreLabel;
    private Label _moveLabel;
    private Button _nextButton;

    public override void _Ready()
    {
        _levelManager = GetNode<LevelManager>("/root/LevelManager");
        _scoreManager = GetNode<ScoreManager>("/root/ScoreManager");
        _topScoreLabel = GetNode<Label>("MarginContainer/Panel/MarginContainer/VBoxContainer/Scores/ScoreTop");
        _moveLabel = GetNode<Label>("MarginContainer/Panel/MarginContainer/VBoxContainer/Scores/Moves");
        _scoreLabel = GetNode<Label>("MarginContainer/Panel/MarginContainer/VBoxContainer/Scores/ScoreCurrent");
        _nextButton = GetNode<Button>("MarginContainer/Panel/MarginContainer/VBoxContainer/Buttons/NextButton");
        UpdateScore();
        UpdateMoves();
        UpdateButtons();
        UpdateTopScore();
    }

    public override void _Input(InputEvent @event)
    {
        if (@event.IsActionPressed("ui_accept"))
        {
            if (!_nextButton.Visible)
            {
                _levelManager.LoadMenuScene();
            }
            else
            {
                _levelManager.LoadNextLevel();
            }
        }
    }

    private void UpdateButtons()
    {
        if (!_levelManager.HasNextLevel())
        {
            _nextButton.Hide();
        }
    }

    private void UpdateScore()
    {
        _scoreLabel.Text = "Score: " + _scoreManager.GetScore();
    }

    private void UpdateMoves()
    {
        _moveLabel.Text = "Moves: " + _scoreManager.GetMoves();
    }

    private void UpdateTopScore()
    {
        _topScoreLabel.Text = "Top Score: " + _scoreManager.GetTopScore();
    }

    public void OnBackButtonPressed()
    {
        _levelManager.LoadMenuScene();
    }

    public void OnRestartButtonPressed()
    {
        _levelManager.RestartLevel();
    }

    public void OnNextButtonPressed()
    {
        _levelManager.LoadNextLevel();
    }
}
