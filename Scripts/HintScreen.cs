using System;
using Godot;

public class HintScreen : Control
{
    public static event Action OnHintShown, OnHintDismissed;

    private Label _hintLabel;
    private string _hintText;

    public override void _Ready()
    {
        _hintLabel = GetNode<Label>("MarginContainer/Panel/MarginContainer/VBoxContainer/Message");
        _hintLabel.Text = _hintText;
        OnHintShown?.Invoke();
    }

    public void SetHint(string hint)
    {
        _hintText = hint;
    }

    public override void _Input(InputEvent @event)
    {
        if (@event.IsActionPressed("ui_accept"))
        {
            OnCloseButtonPressed();
        }
    }

    public void OnCloseButtonPressed()
    {
        OnHintDismissed?.Invoke();
        QueueFree();
    }
}
