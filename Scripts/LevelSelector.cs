using Godot;
using System;

public class LevelSelector : Control
{
    private LevelManager _levelManager;
    private Label _levelLabel;

    private string _initLabel;
    private bool _needsInit;

    public override void _Ready()
    {
        _levelManager = GetNode<LevelManager>("/root/LevelManager");
        _levelLabel = GetNode<Label>("Button/Label");

        if (_needsInit)
        {
            _levelLabel.Text = _initLabel;
        }
    }

    public void OnButtonPressed()
    {
        _levelManager.LoadLevel(_levelLabel.Text);
        GetTree().ChangeScene(AssetReference.GameScene);
    }

    public void SetLabel(string label)
    {
        if (_levelLabel != null)
        {
            _levelLabel.Text = label;
        }
        else
        {
            _initLabel = label;
            _needsInit = true;
        }
    }
}
