using System;
using Godot;
using Simplicity;

public partial class Block : Sprite, IBlockWrapper
{
	public static event Action OnBlockScorePoints;
	public static event Action OnMoveStarted;
	public static event Action OnMoveFinished;
	public static event Action OnBlockRemoved;

	private ScoreManager _scoreManager;

	private int _imageSize = 64;

	private int _moveSpeed = 10;
	private Vector2 _moveFrom;
	private Vector2 _moveTo;
	private bool _isMoving = false;

	private bool _removeWhenFinishedMoving = false;

	private bool _isMerging = false;


	private Label _points;

	public override void _Ready()
	{
		_scoreManager = GetNode<ScoreManager>("/root/ScoreManager");

		_points = GetNode<Label>("Points");
		UpdatePointsText();
	}

	private void UpdatePointsText()
	{
		_points.Text = _scoreManager.GetPointsPerBlock().ToString();
	}

	public void MoveBlock(BlockPosition position)
	{
		if (_isMoving)
		{
			return;
		}
		_moveTo = new Vector2(position.x * _imageSize, position.y * _imageSize);
		OnMoveStarted?.Invoke();
		_isMoving = true;
	}

	public override void _Process(float delta)
	{
		if (!_isMoving)
		{
			return;
		}

		Position = Position.LinearInterpolate(_moveTo, _moveSpeed * delta);

		// stop moving if we are close enough
		if (Position.DistanceTo(_moveTo) < 2)
		{
			Position = _moveTo;
			_isMoving = false;
			OnMoveFinished?.Invoke();
			UpdatePointsText();

			if (_removeWhenFinishedMoving)
			{
				DoRemoveBlock();
			}
		}
	}

	public void ToggleTexture()
	{
		_isMerging = !_isMerging;
		if (_isMerging)
		{
			Texture = ResourceLoader.Load(AssetReference.BlockMergeTexture) as Texture;
		}
		else
		{
			Texture = ResourceLoader.Load(AssetReference.BlockTexture) as Texture;
		}
	}

	public void RemoveBlock()
	{
		if (!_removeWhenFinishedMoving)
		{
			OnBlockScorePoints?.Invoke();
		}
		_removeWhenFinishedMoving = true;
	}

	private void DoRemoveBlock()
	{
		OnBlockRemoved?.Invoke();
		QueueFree();
	}

	public override void _EnterTree()
	{
		Game.OnTogglePlayMode += ToggleTexture;
	}

	public override void _ExitTree()
	{
		Game.OnTogglePlayMode -= ToggleTexture;
	}
}
