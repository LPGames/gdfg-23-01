using System;
using System.Collections;
using Godot;
using Simplicity;

public enum MoveDirection
{
    Left,
    Right,
    Up,
    Down
}

public enum GameSelectionMode
{
    Load = 0,
    Random = 1,
}

public enum GamePlayMode
{
    Block = 0,
    Unblock = 1,
    Ghost = 2,
}

public class Game : SceneBase
{
    public static event Action OnTogglePlayMode;


    private GameManager _gameManager;

    private ConfigManager _configManager;
    private LevelManager _levelManager;
    private ScoreManager _scoreManager;

    private Vector2 _swipePositionStart, _swipePositionEnd;
    private int _minDragDistance = 50;

    private bool _isMoving => _movingBlocks.Count > 0;
    private Queue _movingBlocks = new Queue();

    private bool _showingHint = false;

    private bool _gameOver => _playerBlocks.Count == 0;
    private Queue _playerBlocks = new Queue();

    private GamePlayMode _playMode = GamePlayMode.Block;


    public override void _Ready()
    {
        _configManager = GetNode<ConfigManager>("/root/ConfigManager");
        _levelManager = GetNode<LevelManager>("/root/LevelManager");
        _scoreManager = GetNode<ScoreManager>("/root/ScoreManager");

        _gameManager = new GameManager();
        if (_levelManager.GetGameState() == GameState.Random)
        {
            GenerateNewLevel();
        }
        else
        {
            LoadLevelFromFile();
        }
    }

    public override void _Process(float delta)
    {
        // @todo Update on viewport resize.
        // UpdateCameraZoom();
    }

    private void LoadLevelFromFile()
    {
        string path = _levelManager.GetCurrentLevel();

        // Check if file exists before attempting to read.
        var file = new File();
        if (!file.FileExists(path))
        {
            GenerateNewLevel();
            return;
        }

        _gameManager.LoadLevelFromFile(path);
    }

    private void GenerateNewLevel()
    {
        _gameManager.SetupLevel(9, 15);
        _gameManager.AddWinBlock(7, 13);
        _gameManager.AddRandomWalls(10);
        _gameManager.AddRandomBlocks(5);
        _gameManager.SaveLevel();
    }

    public override void _Input(InputEvent inputEvent)
    {
        if (inputEvent.IsActionPressed("escape"))
        {
            _levelManager.LoadMenuScene();
        }

        if (inputEvent.IsActionPressed("restart"))
        {
            _levelManager.RestartLevel();
        }

        if (_gameOver)
        {
            return;
        }

        if (_showingHint)
        {
            return;
        }

        if (_isMoving)
        {
            return;
        }

        if (inputEvent.IsActionPressed("move_up"))
        {
            // GD.Print("up");
            MoveBlocks(MoveDirection.Up);
            GetTree().SetInputAsHandled();
        }
        if (inputEvent.IsActionPressed("move_down"))
        {
            // GD.Print("down");
            MoveBlocks(MoveDirection.Down);
            GetTree().SetInputAsHandled();
        }
        if (inputEvent.IsActionPressed("move_left"))
        {
            // GD.Print("left");
            MoveBlocks(MoveDirection.Left);
            GetTree().SetInputAsHandled();
        }
        if (inputEvent.IsActionPressed("move_right"))
        {
            // GD.Print("right");
            MoveBlocks(MoveDirection.Right);
            GetTree().SetInputAsHandled();
        }

        if (inputEvent.IsActionPressed("toggle"))
        {
            SwitchGameMode();
            GetTree().SetInputAsHandled();
        }

        if (inputEvent.IsActionPressed("click"))
        {
            // get global mouse position
            _swipePositionStart = GetViewport().GetMousePosition();
        }
        else if (inputEvent.IsActionReleased("click"))
        {
            _swipePositionEnd = GetViewport().GetMousePosition();
            CalculateSwipe();
        }
    }

    private void MoveBlocks(MoveDirection direction)
    {
        _gameManager.MoveBlocks(direction);
    }

    private void BlockCreated(GameBlock _)
    {
        _playerBlocks.Enqueue("block");
    }

    private void BlockRemoved()
    {
        _playerBlocks.Dequeue();

        if (_gameOver)
        {
            OnWin();
        }
    }

    private void BlockMoveStarted()
    {
        _movingBlocks.Enqueue("block");
    }

    private void BlockMoveFinished()
    {
        _movingBlocks.Dequeue();
    }

    private void SwitchGameMode()
    {
        OnTogglePlayMode?.Invoke();
        if (_playMode == GamePlayMode.Block)
        {
            _playMode = GamePlayMode.Unblock;
        }
        else
        {
            _playMode = GamePlayMode.Block;
        }

        _gameManager.SetPlayMode(_playMode);
    }


    private void OnWin()
    {
        _scoreManager.SubmitTopScore();
        var winScreen = GD.Load<PackedScene>(AssetReference.WinScreenPrefab).Instance();
        AddChild(winScreen);
    }

    private void HintIsShowing()
    {
        _showingHint = true;
    }

    private void HintIsClosed()
    {
        _showingHint = false;
    }

    private void CalculateSwipe()
    {
        int x = (int) (_swipePositionEnd.x - _swipePositionStart.x);
        int y = (int) (_swipePositionEnd.y - _swipePositionStart.y);

        if (Math.Abs(x) < _minDragDistance && Math.Abs(y) < _minDragDistance)
        {
            // GD.Print(Math.Abs(x) + " " + Math.Abs(y));
            return;
        }

        if (Math.Abs(x) > Math.Abs(y))
        {
            if (x > 0)
            {
                // GD.Print("swipe right");
                TriggerSwipeEvent((uint) KeyList.Right);
            }
            else
            {
                // GD.Print("swipe left");
                TriggerSwipeEvent((uint) KeyList.Left);
            }
        }
        else
        {
            if (y > 0)
            {
                // GD.Print("swipe down");
                TriggerSwipeEvent((uint) KeyList.Down);
            }
            else
            {
                // GD.Print("swipe up");
                TriggerSwipeEvent((uint) KeyList.Up);
            }
        }
    }

    private void TriggerSwipeEvent(uint scancode)
    {
        var inputEvent = new InputEventKey();
        inputEvent.Pressed = true;
        inputEvent.Scancode = scancode;
        Input.ParseInputEvent(inputEvent);
    }

    public override void _EnterTree()
    {
        base._EnterTree();
        GameArena.OnBlockCreated += BlockCreated;
        Block.OnBlockRemoved += BlockRemoved;
        Interface.OnToggleButtonPressed += SwitchGameMode;
        Block.OnMoveStarted += BlockMoveStarted;
        Block.OnMoveFinished += BlockMoveFinished;
        HintScreen.OnHintShown += HintIsShowing;
        HintScreen.OnHintDismissed += HintIsClosed;
    }

    public override void _ExitTree()
    {
        base._ExitTree();
        GameArena.OnBlockCreated -= BlockCreated;
        Block.OnBlockRemoved -= BlockRemoved;
        Interface.OnToggleButtonPressed -= SwitchGameMode;
        Block.OnMoveStarted -= BlockMoveStarted;
        Block.OnMoveFinished -= BlockMoveFinished;
        Block.OnMoveFinished -= BlockMoveFinished;
        HintScreen.OnHintShown -= HintIsShowing;
        HintScreen.OnHintDismissed -= HintIsClosed;
    }
}
