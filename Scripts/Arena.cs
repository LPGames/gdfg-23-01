using Godot;
using Simplicity;

public class Arena : Node2D
{
	private int _imageSize = 64;

	private void CreateBlock(GameBlock block)
	{
		var position = block.GetPosition();
		var blockInstance = (Block) GD.Load<PackedScene>(AssetReference.BlockPrefab).Instance();

		block.SetWrapper(blockInstance);

		Transform2D transform = Transform2D.Identity;
		transform.origin = new Vector2( position.x * _imageSize, position.y * _imageSize);
		transform.Scale = new Vector2(0.5f, 0.5f);
		blockInstance.Transform = transform;
		AddChild(blockInstance);
	}

	private void CreateWinBlock(GameBlock block)
	{
		var position = block.GetPosition();
		var blockInstance = (WinBlock) GD.Load<PackedScene>(AssetReference.WinBlockPrefab).Instance();

		Transform2D transform = Transform2D.Identity;
		transform.origin = new Vector2( position.x * _imageSize, position.y * _imageSize);
		transform.Scale = new Vector2(0.5f, 0.5f);
		blockInstance.Transform = transform;
		AddChild(blockInstance);
	}

	private void CreateWall(GameBlock block)
	{
		var position = block.GetPosition();
		var wall = (Wall) GD.Load<PackedScene>(AssetReference.WallPrefab).Instance();

		block.SetWrapper(wall);


		Transform2D transform = Transform2D.Identity;
		transform.origin = new Vector2( position.x * _imageSize, position.y * _imageSize);
		transform.Scale = new Vector2(0.5f, 0.5f);
		wall.Transform = transform;

		wall.SetCoords(position);

		AddChild(wall);
	}

	public override void _EnterTree()
	{
		GameArena.OnWinBlockCreated += CreateWinBlock;
		GameArena.OnBlockCreated += CreateBlock;
		GameArena.OnWallCreated += CreateWall;
	}

	public override void _ExitTree()
	{
		GameArena.OnWinBlockCreated -= CreateWinBlock;
		GameArena.OnBlockCreated -= CreateBlock;
		GameArena.OnWallCreated -= CreateWall;
	}
}
