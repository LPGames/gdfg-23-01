public static class AssetReference
{
        public static readonly string BlockTexture = "res://Assets/block.svg";
        public static readonly string BlockMergeTexture = "res://Assets/block-merge.svg";
        public static readonly string WallTexture = "res://Assets/wall.svg";
        public static readonly string WallCrackedTexture = "res://Assets/wall-cracked.svg";
        public static readonly string WinTexture = "res://Assets/win.svg";

        public static readonly string BlockPrefab = "res://Prefabs/Block.tscn";
        public static readonly string WinBlockPrefab = "res://Prefabs/WinBlock.tscn";
        public static readonly string WallPrefab = "res://Prefabs/Wall.tscn";
        public static readonly string WinScreenPrefab = "res://Prefabs/WinScreen.tscn";
        public static readonly string HintScreenPrefab = "res://Prefabs/HintScreen.tscn";
        public static readonly string LevelSelectorPrefab = "res://Prefabs/LevelSelector.tscn";

        public static readonly string MenuScene = "res://Scenes/Menu.tscn";
        public static readonly string GameScene = "res://Scenes/Game.tscn";
        public static readonly string LevelSelectionScene = "res://Scenes/LevelSelection.tscn";
        public static readonly string TutorialLevelPath = "res://Levels/Tutorial/";
        public static readonly string GameLevelPath = "res://Levels/Game/";
}
