using Godot;

public class Menu : UISceneBase
{
    private ConfigManager _configManager;
    private LevelManager _levelManager;

    public override void _Ready()
    {
        _configManager = GetNode<ConfigManager>("/root/ConfigManager");
        _levelManager = GetNode<LevelManager>("/root/LevelManager");
    }

    public override void _Input(InputEvent inputEvent)
    {
        if (inputEvent.IsActionPressed("escape"))
        {
            GetTree().Quit();
        }
    }

    public void OnRandomButtonPressed()
    {
        _levelManager.StartRandom();
        _levelManager.LoadNextLevel();
    }

    public void OnStartButtonPressed()
    {
        _levelManager.StartGame();
        _levelManager.LoadLevelSelectionScene();
    }

    public void OnTutorialButtonPressed()
    {
        _levelManager.StartTutorial();
        _levelManager.LoadLevelSelectionScene();
    }

    public void OnExitButtonPressed()
    {
        GetTree().Quit();
    }
}
