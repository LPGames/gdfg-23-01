using Godot;
using Simplicity;

public partial class Wall : Sprite, IBlockWrapper
{
    private bool _willCrackNextFrame = false;
    private bool _cracked = false;

    private bool _debug = false;

    private Label _coord;
    private string _coordText;
    private bool _setCoordNextFrame = false;

    public override void _Ready()
    {
        _coord = GetNode<Label>("Coord");

        if (!_debug)
        {
            _coord.Hide();
        }

    }

    public override void _Process(float delta)
    {
        if (_willCrackNextFrame)
        {
            DoCrack();
            _willCrackNextFrame = false;
        }

        if (_setCoordNextFrame)
        {
            DoSetCoords();
            _setCoordNextFrame = false;
        }
    }

    public bool IsCracked()
    {
        return _cracked;
    }

    public void Crack()
    {
        _willCrackNextFrame = true;
    }

    public void DoCrack()
    {
        _cracked = true;
        Texture = GD.Load<Texture>(AssetReference.WallCrackedTexture);
    }

    public void MoveBlock(BlockPosition position)
    {
    }

    public void RemoveBlock()
    {
        QueueFree();
    }

    public void SetCoords(BlockPosition blockPosition)
    {
        _setCoordNextFrame = true;
        _coordText = blockPosition.x + "/" + blockPosition.y;
    }

    private void DoSetCoords()
    {
        _coord.Text = _coordText;
    }
}
