using System;
using Godot;

public class UISceneBase : Control
{
    public static event Action<string> OnSceneChange;

    public override void _EnterTree()
    {
       OnSceneChange?.Invoke("/root/Control");
    }

    public override void _ExitTree()
    {
        // @todo Do I need this?
    }
}
