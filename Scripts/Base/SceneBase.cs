using System;
using System.Collections;
using Godot;
using Simplicity;


public class SceneBase : Node2D
{
    public static event Action<string> OnSceneChange;

    public override void _EnterTree()
    {
       OnSceneChange?.Invoke("/root/Game");
    }

    public override void _ExitTree()
    {
        // @todo Do I need this?
    }
}
