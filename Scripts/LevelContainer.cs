using System.Collections.Generic;
using Godot;

public class LevelContainer : GridContainer
{
    private bool _debug = false;

    private LevelManager _levelManager;

    public override void _Ready()
    {
        if (_debug)
        {
            for (int i = 0; i < 40; i++)
            {
                AddLevel((i + 1).ToString());
            }

            return;
        }

        _levelManager = GetNode<LevelManager>("/root/LevelManager");
        List<string> levels = _levelManager.GetLevels();

        for (int i = 0; i < levels.Count; i++)
        {
            AddLevel(levels[i]);
        }
    }

    public void AddLevel(string label)
    {
        var level = (LevelSelector) GD.Load<PackedScene>(AssetReference.LevelSelectorPrefab).Instance();
        level.SetLabel(label);
        AddChild(level);
    }
}
