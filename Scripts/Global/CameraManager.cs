using Godot;
using System;

public class CameraManager : Node
{
    private Camera2D _camera;
    private Viewport _viewport;

    private string _rootNamespace;
    private bool _initCamera = false;

    public override void _Process(float delta)
    {
        if (!_initCamera)
        {
            return;
        }

        GD.Print("camera update");

        _camera = GetNode<Camera2D>(_rootNamespace + "/Camera2D");
        _viewport = GetViewport();
        UpdateCameraZoom();

        _initCamera = false;
    }

    private void OnSceneChamge(string rootNamespace)
    {
        GD.Print("scene changed");
        _rootNamespace = rootNamespace;
        _initCamera = true;
    }

    public void UpdateCameraZoom()
    {
        if (_camera == null)
        {
            GD.PrintErr("No Camera found in scene.");
            return;
        }


#if GODOT_WEB
        var x = (float) JavaScript.Eval("Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0)");
        var y = (float) JavaScript.Eval("Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0)");
        var viewportSize = new Vector2(x, y);
#else
        var viewportSize = _viewport.GetVisibleRect().Size;
#endif
        // GD.Print(viewportSize.x);

        var zoom = Math.Min(viewportSize.x / 576, viewportSize.y / 1024);

        if (zoom >= 1)
        {
            return;
        }

        _camera.Zoom = new Vector2(1 / zoom, 1 / zoom);



        GD.Print(viewportSize.x);
        GD.Print(viewportSize.y);
        GD.Print(zoom);

        GD.Print(576 * zoom);
        GD.Print(1024 * zoom);


        if (viewportSize.x > 576 * zoom)
        {
            Transform2D transform = _camera.Transform;
            transform.origin.x = -((viewportSize.x - 576 * zoom) / 2) / zoom;
            _camera.Transform = transform;
        }

        if (viewportSize.y > 1024 * zoom)
        {
            Transform2D transform = _camera.Transform;
            transform.origin.y = -((viewportSize.y - 1024 * zoom) / 2) / zoom;
            _camera.Transform = transform;
        }
    }

    public override void _EnterTree()
    {
        SceneBase.OnSceneChange += OnSceneChamge;
        // UISceneBase.OnSceneChange += OnSceneChamge;
    }

    public override void _ExitTree()
    {
        SceneBase.OnSceneChange -= OnSceneChamge;
        // UISceneBase.OnSceneChange -= OnSceneChamge;
    }
}
