using Godot;
using System;

public class ConfigManager : Node
{
    private ConfigFile _config;
    private string _configPath = "user://settings.cfg";

    public int GetGameSelectionMode()
    {
        return (int) _config.GetValue("game", "mode", GameSelectionMode.Random);
    }

    public override void _Ready()
    {
        _config = new ConfigFile();
        var error = _config.Load(_configPath);

        // check if config file exists
        if (error != Error.Ok)
        {
            _config.SetValue("game", "mode", GameSelectionMode.Random);
            _config.Save(_configPath);
        }
    }

    public void SetGameSelectionMode(GameSelectionMode selectionMode)
    {
        _config.SetValue("game", "mode", selectionMode);
        _config.Save(_configPath);
    }
}
