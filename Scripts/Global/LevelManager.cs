using Godot;
using System.Collections.Generic;
using System.Linq;
using Simplicity;

public enum GameState
{
    Random = 0,
    Tutorial = 1,
    Game = 2,
}

public class LevelManager : Node
{
    private ScoreManager _scoreManager;

    private Queue<string> _gameLevels;
    private Queue<string> _tutorialLevels;
    private GameState _gameState;

    private string _currentLevelLabel;
    private string _currentLevel = "user://arena.json";

    public override void _Ready()
    {
        _scoreManager = GetNode<ScoreManager>("/root/ScoreManager");
    }

    public string GetCurrentLevel()
    {
        return _currentLevel;
    }

    public GameState GetGameState()
    {
        return _gameState;
    }

    private void QueueTutorialLevels()
    {
        _tutorialLevels = new Queue<string>();

        List<string> levelFiles = new List<string>();
        var directory = new Directory();
        directory.Open(AssetReference.TutorialLevelPath);
        directory.ListDirBegin(true, true);
        while (true)
        {
            var file = directory.GetNext();
            if (file == "")
            {
                break;
            }

            if (file.EndsWith(".json"))
            {
                levelFiles.Add(file.Substring(0, file.Length - 5));
            }
        }

        if (levelFiles.Count == 0)
        {
            GD.Print("No tutorial levels found");
            return;
        }

        // Sort list of level files
        levelFiles.Sort();

        foreach (string levelFile in levelFiles)
        {
            _tutorialLevels.Enqueue(levelFile);
        }
    }

    private void QueueGameLevels()
    {
        _gameLevels = new Queue<string>();

        List<string> levelFiles = new List<string>();
        var directory = new Directory();
        directory.Open(AssetReference.GameLevelPath);
        directory.ListDirBegin(true, true);
        while (true)
        {
            var file = directory.GetNext();
            if (file == "")
            {
                break;
            }

            if (file.EndsWith(".json"))
            {
                levelFiles.Add(file.Substring(0, file.Length - 5));
            }
        }

        if (levelFiles.Count == 0)
        {
            GD.Print("No game levels found");
            return;
        }

        // Sort list of level files
        levelFiles.Sort();

        foreach (string levelFile in levelFiles)
        {
            _gameLevels.Enqueue(levelFile);
        }
    }

    public List<string> GetLevels()
    {
        if (_gameState == GameState.Tutorial)
        {
            return _tutorialLevels.ToList();
        }
        return _gameLevels.ToList();
    }

    public void LoadLevel(string levelLabelText)
    {
        _currentLevelLabel = null;
        Queue<string> queue;
        string levelPath;
        if (_gameState == GameState.Tutorial)
        {
            StartTutorial();
            queue = _tutorialLevels;
            levelPath = AssetReference.TutorialLevelPath;
        }
        else
        {
            StartGame();
            queue = _gameLevels;
            levelPath = AssetReference.GameLevelPath;
        }

        while (_currentLevelLabel != levelLabelText && queue.Count > 0)
        {
            _currentLevelLabel = queue.Dequeue();
        }

        if (_currentLevelLabel == levelLabelText)
        {
            _currentLevel = levelPath + _currentLevelLabel + ".json";

            _scoreManager.SetCurrentLevel(levelLabelText);
            _scoreManager.ResetScore();

            GetTree().ChangeScene(AssetReference.GameScene);
        }
        else
        {
            GD.PrintErr("Level not found.");
        }
    }

    public void LoadNextLevel()
    {
        string level;
        switch (_gameState)
        {
            case GameState.Random:
                _currentLevel = "";
                break;
            case GameState.Tutorial:
                level = _tutorialLevels.Dequeue();
                _currentLevel = AssetReference.TutorialLevelPath + level + ".json";
                break;
            case GameState.Game:
                level = _gameLevels.Dequeue();
                _scoreManager.SetCurrentLevel(level);
                _currentLevel = AssetReference.GameLevelPath + level + ".json";
                // _currentLevel = "user://arena.json";
                break;

        }

        _scoreManager.ResetScore();
        GetTree().ChangeScene(AssetReference.GameScene);
    }

    public void RestartLevel()
    {
        if (_gameState == GameState.Random)
        {
            StartGame();
        }
        _scoreManager.ResetScore();
        GetTree().ChangeScene(AssetReference.GameScene);
    }

    public void StartTutorial()
    {
        _gameState = GameState.Tutorial;
        QueueTutorialLevels();
    }

    public void StartRandom()
    {
        _gameState = GameState.Random;
    }

    public void StartGame()
    {
        _gameState = GameState.Game;
        QueueGameLevels();
    }


    public bool HasNextLevel()
    {
        if (_gameState == GameState.Tutorial)
        {
            return _tutorialLevels.Count > 0;
        }
        if (_gameState == GameState.Game)
        {
            return _gameLevels.Count > 0;
        }
        return false;
    }

    public void LoadLevelSelectionScene()
    {
        GetTree().ChangeScene(AssetReference.LevelSelectionScene);
    }

    public void LoadMenuScene()
    {
        GetTree().ChangeScene(AssetReference.MenuScene);
    }
}
