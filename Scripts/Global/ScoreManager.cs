using Godot;
using System;
using System.Collections.Generic;
using Simplicity;

public class ScoreManager : Node
{
    private int _score = 0;
    private int _moves = 0;
    private string _currentLevel;

    private int _startingPointsPerMove = 250;
    private int _pointReductionPerMove = 50;
    private int _finalPointReduction = 10;
    private int _pointsPerBlock;

    private Dictionary<string, int> _topScores = new Dictionary<string, int>();

    public int GetScore()
    {
        return _score;
    }

    public int GetMoves()
    {
        return _moves;
    }

    public int GetPointsPerBlock()
    {
        return _pointsPerBlock;
    }


    public void ResetScore()
    {
        _pointsPerBlock = _startingPointsPerMove;
        _score = 0;
    }

    public void SetCurrentLevel(string level)
    {
        _currentLevel = level;
    }

    public int GetTopScore()
    {
        if (_topScores.ContainsKey(_currentLevel))
        {
            return _topScores[_currentLevel];
        }
        else
        {
            return 0;
        }
    }

    private void OnMovesChanged(int moves)
    {
        _moves = moves;
    }

    public void SubmitTopScore()
    {
        int currentTopScore = GetTopScore();
        if (currentTopScore == 0 || _score > currentTopScore)
        {
            _topScores[_currentLevel] = _score;
        }
    }

    public override void _EnterTree()
    {
        Interface.OnMovesChanged += OnMovesChanged;
        GameArena.OnBlocksMoved += ReducePoints;
        Block.OnBlockScorePoints += ScorePoints;
    }

    private void ScorePoints()
    {
        _score += _pointsPerBlock;
    }

    private void ReducePoints()
    {
        if (_pointsPerBlock > _pointReductionPerMove)
        {
            _pointsPerBlock = _pointsPerBlock - _pointReductionPerMove;
            return;
        }

        if (_pointsPerBlock > _finalPointReduction)
        {
            _pointsPerBlock = _pointsPerBlock - _finalPointReduction;
        }
    }

    public override void _ExitTree()
    {
        Interface.OnMovesChanged -= OnMovesChanged;
        GameArena.OnBlocksMoved -= ReducePoints;
        Block.OnBlockScorePoints -= ScorePoints;
    }
}
